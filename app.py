from flask import Flask, request, send_file
from os import environ

import pypandoc
import tempfile
import logging

app = Flask(__name__)

base_template = \
"""
<html>

<head>
  <title>quick document converter</title>
</head>

<body>
{content}
</body>

</html>
"""

@app.route("/conv", methods=['POST'])
def conv():
    _v = request.values.get('conv-val', '')
    _from = request.values.get('conv-from', 'md')
    _to = request.values.get('conv-to', 'md')
    _extra = ['-V', 'geometry:margin=1in'] if request.values.get('small-margins', False) else []

    # check if there was a file attached
    if _from_file := request.files.get('conv-from-file'):
      _v = _from_file.stream.read().decode()

    _, f_name = tempfile.mkstemp(
        suffix="." + _to.replace(".", "")
    )

    pypandoc.convert_text(
        _v,
        _to,
        format=_from,
        outputfile=f_name,
	extra_args=_extra,
    )

    return send_file(f_name)

@app.route("/")
def index():
	return base_template.format(content=\
"""
<form action='/conv' method='POST'">
  <textarea name='conv-val' id='conv-val' rows='20' cols='60'>Hello, world!</textarea>
  <br>
  <br>
  <label for='conv-from'>Convert from:</label>
  <select name='conv-from' id='conv-from'>
    <option value='md'>Markdown</option>
    <option value='rst'>reStructuredText</option>
    <option value='rtf'>Rich Text Format</option>
    <option value='t2t'>txt2tags</option>
    <option value='latex'>LaTeX</option>
  </select>
  <br>
  <label for='conv-to'>Convert to:</label>
  <select name='conv-to' id='conv-to'>
    <option value='pdf'>PDF</option>
    <option value='docx'>Docx</option>
    <option value='odt'>ODT</option>
    <option value='md'>Markdown</option>
  </select>
  <br>
  <input type='submit' value='submit'>
</form>
""")

if __name__ == "__main__":
	port = int(environ.get("PORT", 8080))
	debug = bool(environ.get("DEBUG", False))
	app.run(debug=debug, host="0.0.0.0", port=port)

