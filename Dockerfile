FROM python:alpine

RUN apk add pandoc texlive
RUN pip install Flask pypandoc

COPY *.py .

CMD ["python",  "/app.py"]

